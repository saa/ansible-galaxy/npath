# NPath / Direct Server Return

This Ansible role configures IPv4/IPv6 addresses for direct return routing.
This allows load balancers to split traffic among multiple servers configured
with the same service address whose return traffic is sent directly to the
requesting client without it going back through the load balancer.

## Installation

Add the following to a `requirements.yml` or `requirements.yaml` file in your
Ansible project and run `ansible-galaxy install -r requirements.yaml`:

```yaml
collections:
  - community.general

roles:
  - name: npath
    scm: git
    src: https://gitlab.uvm.edu/saa/ansible-galaxy/npath.git
```

## Usage

Place a section like this in your playbook:

```yaml
- name: Configure npath service addresses
  hosts: all
  roles:
    - role: npath
      npath_ipv4_addresses:
        - [..-]
      npath_ipv6_addresses:
        - [...]
```
### Additional options

Full argument documentation is available via `ansible-doc -t role npath`.
